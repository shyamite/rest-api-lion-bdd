package com.tenx.testing.framework.bdd.test.stepdefs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;

import com.tenx.testing.framework.bdd.test.helpers.Constants;
import com.tenx.testing.framework.bdd.test.helpers.GeneralHelper;
import com.tenx.testing.framework.bdd.test.helpers.QueueHelper;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import javax.naming.OperationNotSupportedException;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.CollectionUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.tenx.lion.utility.conversion.JsonUtils;
import com.tenx.lion.utility.logging.LionDefaultLoggerFactory;
import com.tenx.lion.utility.logging.LionLogger;
import com.tenx.testing.framework.bdd.test.config.Config;
import com.tenx.testing.framework.bdd.test.config.CucumberConfiguration;
import com.tenx.testing.framework.bdd.test.data.loader.BaseLoader;
import com.tenx.testing.framework.bdd.test.repository.CucumberRepositoryPool;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

@ContextConfiguration(classes = {CucumberConfiguration.class})
public class GetAPIStepDefinitions {

  private Response response;
  private ValidatableResponse json;
  private RequestSpecification request;

  private Map<String, String> parameterMap;
  private Map<String, Object> preConditionMap;

  @Value("${baseUrl}")
  private String baseUrl;

  private Method httpMethod;
  private String apiUrl;
  private JSONObject jsonObject;

  private static final LionLogger LOGGER =
      LionDefaultLoggerFactory.getLogger(GetAPIStepDefinitions.class);

  @Autowired
  private CucumberRepositoryPool cucumberRepositoryPool;
  @Autowired
  private BaseLoader baseLoader;
  @Autowired
  private Config config;
  @Autowired
  private GeneralHelper helper;
  @Autowired
  private QueueHelper queueHelper;

  private String entityName;
  private List<Object> loadedResources;

  @PostConstruct
  public void initialize() {
    parameterMap = new HashMap<String, String>();
    preConditionMap = new HashMap<String, Object>();
    loadedResources = new ArrayList<>();
  }

  //TODO: Ideally we want to remove the after here. The delete should remove all data by tenant

  @Before
  public void cleanBeforeScenario() {
    clearResources();
  }

  @After
  public void cleanAfterScenario() {
    clearResources();
  }

  @Given("^api is ([^\"]*)$")
  public void givenApiIs(String apiName) {
    request = given().baseUri(baseUrl);
    String httpMethodKey = apiName + Constants.HTTP_METHOD;
    String apiUrlKey = apiName + Constants.URL;

    this.httpMethod = Method.valueOf(config.get(httpMethodKey));
    this.apiUrl = config.get(apiUrlKey);
  }

  @Given("^api call of ([^\"]*) with ([^\"]*)$")
  public void givenApi(String apiName , String baseURL) {
    request = given().baseUri(baseURL);
    String httpMethodKey = apiName + Constants.HTTP_METHOD;
    String apiUrlKey = apiName + Constants.URL;

    this.httpMethod = Method.valueOf(config.get(httpMethodKey));
    this.apiUrl = config.get(apiUrlKey);
  }

  @Given("^json file name is ([^\"]*)$")
  public void jsonFileNameIs(String fileName) throws IOException, JSONException {
    String jsonPath = baseLoader.getJsonPath(fileName);
    jsonObject = baseLoader.readJsonFileAsObject(jsonPath, fileName);
    request.header(Constants.CONTENT_TYPE, Constants.JSON_APPLICATION).body(jsonObject);
  }


  @Given("^path[\\s]+parameter[\\s]+([^\"]*)[\\s]+is[\\s]+([\"]*.*[\"]*)$")
  public void parameterIs(String parameterName, String parameterValue) {
    parameterMap.put(parameterName.trim(), helper.getKey(parameterValue));
  }

  @SuppressWarnings("unchecked")
  @Given("^pre-condition[\\s]+insert[\\s]+([^\"]*)[\\s]+of[\\s]+type[\\s]+([^\\\"]*)[\\s]+into[\\s]+database[\\s]+from[\\s]+([\"]*.*[\"]*)$")
  public void preConditionInsert(String entityName, String entityType, String valueSource)
      throws IllegalArgumentException, IOException, ClassNotFoundException {
    String classType = baseLoader.getFullyQualifiedClassPath(entityType);
    if (classType == null) {
      LOGGER.info("class name is null.");
      throw new IllegalArgumentException("Unable to get Class Type for " + entityType);
    }
    try {
      if (valueSource.toLowerCase(Locale.ENGLISH).matches(Constants.MEMORY)) {
        readFromMemory(entityName, entityType);
      } else if (valueSource.toLowerCase(Locale.ENGLISH).endsWith(Constants.JSON_FILE)) {
        Object entity = baseLoader.loadEntityFromJsonFile(entityType, valueSource);
        preConditionMap.put(entityName, entity);
        helper.logFailedItems(cucumberRepositoryPool.saveItem(entity));
      }
    } catch (IOException | ClassNotFoundException ex) {
      LOGGER.error("Error in Pre-Condition Insert : ", ex);
      throw ex;
    }
  }

  @Given("^pre-condition[\\s]+write[\\s]+([^\"]*)[\\s]+of[\\s]+type[\\s]+([^\"]*)[\\s]+into[\\s]+memory[\\s]+from[\\s]+([\"]*.*[\"]*)$")
  public void preConditionWrite(String entityName, String entityType, String valueSource)
      throws IOException, ClassNotFoundException {
    if (baseLoader.getFullyQualifiedClassPath(entityType) != null) {
      try {
        preConditionMap.put(entityName, baseLoader.loadEntityFromJson(entityType));
      } catch (IOException | ClassNotFoundException ex) {
        LOGGER.error("Error in Pre-Condition Write : ", ex);
        throw ex;
      }
    }
  }

  @Given("^pre-condition[\\s]+assign[\\s]+([^\"]*)[\\s]+to[\\s]+([\"]*.*[\"]*)$")
  public void preConditionAssign(String entityName, String valueSource)
      throws JSONException, ClassNotFoundException {
    String[] keys = entityName.split("\\.");
    if (preConditionMap.containsKey(keys[0])) {
      try {
        Class<?> className =
            baseLoader.getClassInstance(preConditionMap.get(keys[0]).getClass().getSimpleName());
        JSONObject jsonObj =
            new JSONObject(JsonUtils.jsonToString(preConditionMap.get(keys[0])));
        String elementToChange = keys[keys.length - 1];

        if (elementToChange != null && jsonObj.has(elementToChange)) {
          helper.assignValueToField(jsonObj, elementToChange, valueSource);
        }
        Object entityObject = JsonUtils.stringToJson(jsonObj.toString(), className);
        preConditionMap.put(keys[0], entityObject);
      } catch (JSONException | ClassNotFoundException ex) {
        LOGGER.error("Error in Pre-Condition Assign : ", ex);
        throw ex;
      }
    }
  }

  @When("^the client makes api call$")
  public void whenTheClientMakesApiCall() throws MalformedURLException {
    String apiUrlWithParams = apiUrl;

    for (Map.Entry keyValue : parameterMap.entrySet()) {
      apiUrlWithParams = apiUrlWithParams.replace("{" + keyValue.getKey() + "}", keyValue.getValue().toString());
    }

    String fullUrl = baseUrl + apiUrlWithParams;

    URL url = new URL(fullUrl);
    response = request.when().request(httpMethod, url);
    LOGGER.info("Response from the API Call:");
    response.prettyPrint();
  }

  @Then("the status code is (\\d+)")
  public void verifyStatusCode(int statusCode) {
    json = response.then().statusCode(statusCode);
  }

  @Then("response includes the following$")
  public void responseContainsValues(Map<String, String> responseFields) throws Exception {
    jsonObject = new JSONObject(json.extract().asString());
    helper.validateResponseFields(responseFields, jsonObject);
  }

  @Then("response includes the following in any order")
  public void responseContainsInAnyOrder(Map<String, String> responseFields) {
    for (Map.Entry<String, String> field : responseFields.entrySet()) {
      if (StringUtils.isNumeric(field.getValue())) {
        json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
      } else {
        json.body(field.getKey(), containsInAnyOrder(field.getValue()));
      }
    }
  }

  @Given("^assign jsonbody attribute ([^\"]*) with (\"[^\"]*\")$")
  public void jsonBodyDynamicAttributeChange(String jsonAttributePath, String jsonAttributeNewValue)
      throws JSONException {
    jsonAttributeNewValue = helper.getKey(jsonAttributeNewValue);
    request
        .header(Constants.CONTENT_TYPE, Constants.JSON_APPLICATION)
        .body(helper
            .assignJsonBodyAttributeChange(jsonAttributePath, jsonAttributeNewValue, jsonObject));
  }

  @Given("^assign jsonbody attribute ([^\"]*) to ([^\"]*)$")
  public void jsonBodyAttributeChange(String jsonAttributePath, String jsonAttributeNewValue)
      throws JSONException {
    request
        .header(Constants.CONTENT_TYPE, Constants.JSON_APPLICATION)
        .body(helper
            .assignJsonBodyAttributeChange(jsonAttributePath, jsonAttributeNewValue, jsonObject));
  }

  @Given("^entity is ([^\"]*)$")
  public void givenEntityIs(String entityName) {
    this.entityName = entityName;
  }

  @When("^the repository is called$")
  public void whenRepositoryIsCalled()
      throws JsonParseException, JsonMappingException, ClassNotFoundException, IOException {
    try {
      Object entityObject = baseLoader.loadEntityFromJson(entityName);
      cucumberRepositoryPool.saveItem(entityObject);
    } catch (IOException | ClassNotFoundException ex) {
      LOGGER.error("Error in When Repository Is Called : ", ex);
      throw ex;
    }
  }

  @When("^saved record is retrieved by key ([^\"]*) and ([^\"]*) of type ([^\"]*)$")
  public void whenSavedRecordIsRetrieved(String hashKey, String rangeKey, String entityName)
      throws JSONException, ClassNotFoundException {
    this.entityName = entityName;

    CucumberRepositoryPool.ID id = new CucumberRepositoryPool.ID();

    id.setHaskKey(hashKey);
    id.setRangeKey(rangeKey);
    Object retrievedResource = cucumberRepositoryPool.retrieve(id, this.entityName);
    loadedResources.add(retrievedResource);
    String jsonString = JsonUtils.toJson(retrievedResource);
    jsonObject = new JSONObject(jsonString);
  }

  @Then("^saved record includes the following$")
  public void thenSavedRecordIsVerified(Map<String, String> responseFields) throws Exception {
    helper.validateResponseFields(responseFields, jsonObject);
  }

  @When("^saved record is retrieved by \"([^\"]*)\" with values \"([^\"]*)\" of type ([^\"]*)$")
  public void whenSavedRecordIsRetrievedByColumns(String columnNames, String columnValues,
      String entityName) throws JSONException, ClassNotFoundException {
    String jsonString = null;
    this.entityName = entityName;

    Map<String, String> columnMap = new HashMap<>(columnNames.length());

    String[] columns = columnNames.split(",");
    String[] values = columnValues.split(",");

    if (columns.length == values.length) {
      for (int index = 0; index < columns.length; index++) {
        columnMap.put(columns[index], values[index]);
      }
    }

    /*
    This code required to ensure there is a delay when checking database records. The reason for this is some services use
    transaction manager and queues. As a result there is a delay in the records being created.
     */
    int i = 0;
    List<Object> retrievedResource = null;

    while(CollectionUtils.isEmpty(retrievedResource) == true && i <= 5){
      retrievedResource =
          cucumberRepositoryPool.retrieveByColumnNames(columnMap, this.entityName);
      try {
        TimeUnit.SECONDS.sleep(5);
      } catch (InterruptedException ex) {
        LOGGER.info("InterruptedException whilst checking record exists:" + ex);
        Thread.currentThread().interrupt();
      }
      i++;
    }

    loadedResources.addAll(retrievedResource);
    if (!CollectionUtils.isEmpty(retrievedResource)) {
      jsonString = JsonUtils.toJson(retrievedResource.get(0));
    }
    jsonObject = new JSONObject(jsonString);
  }


  private void clearResources() {
    loadedResources.addAll(preConditionMap.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList()));
    cucumberRepositoryPool.delete(loadedResources);
    preConditionMap.clear();
    loadedResources.clear();
  }

  private <S> void readFromMemory(String entityName, String classType)
      throws ClassNotFoundException {
    String entityNames[] = entityName.split(",");
    Class<?> className;
    List<S> entities = new ArrayList<S>();
    try {
      className = baseLoader.getClassInstance(classType);
    } catch(ClassNotFoundException ex) {
      LOGGER.error("class " + classType + " is not found", ex);
      throw ex;
    }

    for (int i = 0; i < entityNames.length; i++) {
      if (preConditionMap.get(entityNames[i]) != null &&
          className.isInstance(preConditionMap.get(entityName))) {
        entities.add((S) preConditionMap.get(entityNames[i]));
      }
    }

    if (entityNames.length > 1) {
      helper.logFailedItems(cucumberRepositoryPool.saveItems(entities));
    } else {
      helper.logFailedItems(cucumberRepositoryPool.saveItem(preConditionMap.get(entityName)));
    }
  }


  @Given("^Post[\\s]+a[\\s]+([^\"]*)[\\s]+message[\\s]+from[\\s]+([^\"]*)[\\s]+to[\\s]+([^\"]*)[\\s]+([^\"]*)[\\s]+queue")
  public void postMessageToQueue(String message, String messageSource, String queueName,
      String queueType)
      throws OperationNotSupportedException, JMSException, IOException {
    if ("SQS".equalsIgnoreCase(queueType)) {
      if (!messageSource.endsWith(Constants.JSON_FILE)) {
        throw new OperationNotSupportedException(
            "BDD FW currently supports message of type JSON. Specifed '." + StringUtils
                .substringAfterLast(messageSource, ".") + "' is not implemented yet!!!");
      }
      String messagePath = baseLoader.getJsonPath(message); // Path where the message exists
      String messageContent = baseLoader.readJsonFileAsString(messagePath, messageSource);
      queueHelper.postMessageToSQSQueue(messageContent, baseLoader.getQueueName(queueName));
    }
  }
}
