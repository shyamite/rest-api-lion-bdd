package com.tenx.testing.framework.bdd.test.helpers;

import com.tenx.lion.utility.logging.LionDefaultLoggerFactory;
import com.tenx.lion.utility.logging.LionLogger;
import com.tenx.lion.utility.util.LionGeneratorUtils;
import com.tenx.testing.framework.bdd.test.stepdefs.GetAPIStepDefinitions;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GeneralHelper {

  private static final LionLogger LOGGER =
      LionDefaultLoggerFactory.getLogger(GetAPIStepDefinitions.class);

  private JSONObject jsonObject;

  @Autowired
  private LionGeneratorUtils lionUtils;

  /**
   * Log the failed items
   *
   * @param failedItems failed items
   */
  public <S> void logFailedItems(Map<String, List<S>> failedItems) {
    if (!CollectionUtils.isEmpty(failedItems)) {
      failedItems.values()
          .stream()
          .filter(item -> !CollectionUtils.isEmpty(item))
          .forEach(item -> LOGGER.info(item.toString()));
    }
  }

  /**
   * Validate the response fields.
   *
   * @param responseFields fields that needs to be validated
   * @throws AssertionError when the actual doesnt match with expected.
   * @throws JSONException any JSON exception
   */
  public void validateResponseFields(Map<String, String> responseFields, JSONObject jsonObj)
      throws AssertionError, JSONException {
    this.jsonObject = jsonObj;
    for (Map.Entry<String, String> field : responseFields.entrySet()) {
      boolean assertionError = false;
      String actual = getJsonValue(field.getKey());
      // If the value should be NON-EMPTY then check if the actual value is blank, if yes throw error,
      // else do the actual and expected check
      if (("NON-EMPTY".equalsIgnoreCase(field.getValue()) && StringUtils.isBlank(actual)) ||
          (!"NON-EMPTY".equalsIgnoreCase(field.getValue()) && !StringEscapeUtils
              .unescapeJava(actual).equals(StringEscapeUtils.unescapeJava(field.getValue())))) {
        throw new AssertionError(
            "expected " + field.getKey() + ":'" + field.getValue() + "' but actual '" + actual
                + "'");
      }
    }
  }

  /**
   * Get the value form JSON, given a key.
   *
   * @param keyName key for which the value is required.
   * @return value for the given key
   * @throws JSONException json exception
   */

  private String getJsonValue(String keyName) throws JSONException {
    String jsonValue = null;
    String[] keys = keyName.split(Pattern.quote("."));
    JSONObject jsonObj = jsonObject;
    JSONArray jsonArray;
    Object value;
    for (String key : keys) {
      int index = 0;
      if (key.contains("[") && key.contains("]")) {
        index = getIndexFromArray(key);
        key = key.substring(0, key.length() - 3);
      }
      value = jsonObj.get(key);
      if (isJsonObject(value)) {
        jsonObj = (JSONObject) value;
      } else if (isJsonArray(value)) {
        jsonArray = (JSONArray) value;
        if (!isJsonObject(jsonArray.get(index))) {
          jsonValue = jsonArray.get(index).toString();
          break;
        }
        jsonObj = (JSONObject) jsonArray.get(index);
      } else {
        jsonValue = jsonObj.get(key).toString();
        break;
      }
    }
    return jsonValue;
  }

  /**
   * Check if the passed object is a JSON Object
   *
   * @param obj object to check
   * @return <code>true,</code> if JSON Object, <br><code>false </code>otherwise
   */
  private boolean isJsonObject(Object obj) {
    boolean isJsonObject = false;
    if (obj instanceof JSONObject) {
      isJsonObject = true;
    }
    return isJsonObject;
  }

  /**
   * Check if the passed object is a JSON Array
   *
   * @param obj object to check
   * @return <code>true,</code> if JSON Array, <br><code>false </code>otherwise
   */
  private boolean isJsonArray(Object obj) {
    boolean isJsonArray = false;
    if (obj instanceof JSONArray) {
      isJsonArray = true;
    }
    return isJsonArray;
  }

  /**
   * Get the index from array.
   *
   * @param key key
   * @return array index
   */
  private int getIndexFromArray(String key) {
    return Integer.parseInt(key.substring(key.length() - 2, key.length() - 1));
  }


  /**
   * Get the value for the type of key passed.
   *
   * @param typeOfKey type of key
   * @return key value
   */
  public String getKey(String typeOfKey) {
    String key;
    if (Constants.NUMERIC_RANDOM.equalsIgnoreCase(typeOfKey.trim())) {
      key = lionUtils.generateRandomKey();
    } else if (Constants.ALPHANUMERIC_RANDOM.equalsIgnoreCase(typeOfKey.trim())) {
      key = lionUtils.generateAlphaNumericRandomKey();
    } else if (Constants.UUID.equalsIgnoreCase(typeOfKey.trim())) {
      key = lionUtils.generateUUID();
    } else {
      key = typeOfKey.trim();
    }
    return key;
  }

  /**
   * Assign value to a field
   *
   * @param jsonObj json object where values to be assigned
   * @param elementToChange the element to change
   * @param valueSource the value to be changed
   */
  public void assignValueToField(JSONObject jsonObj, String elementToChange, String valueSource)
      throws JSONException {
    String value = getKey(valueSource);
    try {
      Object jsonValue = jsonObj.get(elementToChange);
      if (jsonValue instanceof Boolean) {
        jsonObj.put(elementToChange, Boolean.valueOf(value));
      } else if (jsonValue instanceof Integer || jsonValue instanceof Long) {
        jsonObj.put(elementToChange, Long.valueOf(value));
      } else if (jsonValue instanceof Float || jsonValue instanceof Double) {
        jsonObj.put(elementToChange, Double.parseDouble(value));
      } else {
        jsonObj.put(elementToChange, value);
      }
    } catch (JSONException ex) {
      LOGGER.error("Error assigning value to field ", ex);
      throw ex;
    }
  }

  public JSONObject assignJsonBodyAttributeChange(String jsonAttributePath,
      String jsonAttributeNewValue, JSONObject jsonObject) throws JSONException {
    String[] jsonAttributes = jsonAttributePath.split(Pattern.quote("."));
    JSONObject jsonObj = jsonObject;
    for (int i = 0; i < jsonAttributes.length - 1; i++) {
      jsonObj = jsonObj.getJSONObject(jsonAttributes[i]);
    }
    jsonObj.put(jsonAttributes[jsonAttributes.length - 1], jsonAttributeNewValue);
    LOGGER.info("json updated response: " + jsonObj.toString());
    return jsonObj;
  }
}