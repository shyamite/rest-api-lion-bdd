package com.tenx.testing.framework.bdd.test.config;

import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import javax.jms.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.destination.DynamicDestinationResolver;

@Configuration
@EnableJms
public class SQSConfig {

  @Value("${amazon.sqs.endpoint:endpoint}")
  private String amazonSqsEndpoint;

  @Value("${amazon.sqs.region:eu-west-1}")
  private String amazonSqsRegion;

  @Value("${fees.queue.name:queue}")
  private String feesQueueName;

  @Value("${sqs.queue.concurrency:3}")
  private String concurrency;


  @Bean
  public AmazonSQS getClient() {
    return AmazonSQSClientBuilder.standard()
        .withCredentials(new DefaultAWSCredentialsProviderChain())
        .withEndpointConfiguration(new EndpointConfiguration(amazonSqsEndpoint, amazonSqsRegion))
        .build();
  }


  @Bean
  public SQSConnectionFactory getSQSConnectionFactory() {
    SQSConnectionFactory sqsConnectionFactory = SQSConnectionFactory.builder()
        .withEndpoint(amazonSqsEndpoint)
        .withRegionName(amazonSqsRegion)
        .withAWSCredentialsProvider(new DefaultAWSCredentialsProviderChain())
        .build();
    return sqsConnectionFactory;
  }

  @Bean
  public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(
      SQSConnectionFactory sqsConnectionFactory) {
    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
    factory.setConnectionFactory(sqsConnectionFactory);
    factory.setDestinationResolver(new DynamicDestinationResolver());
    factory.setConcurrency(concurrency);
    factory.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
    return factory;
  }

  @Bean
  public JmsTemplate defaultJmsTemplate(SQSConnectionFactory sqsConnectionFactory) {
    JmsTemplate jmsTemplate = new JmsTemplate(sqsConnectionFactory);
    jmsTemplate.setDefaultDestinationName(feesQueueName);
    jmsTemplate.setDeliveryPersistent(false);
    return jmsTemplate;
  }
}
