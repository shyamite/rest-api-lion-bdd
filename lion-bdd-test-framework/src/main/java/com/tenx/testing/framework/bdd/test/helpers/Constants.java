package com.tenx.testing.framework.bdd.test.helpers;

import org.springframework.stereotype.Component;

public class Constants {

  // Used in helpers
  public static final String ALPHANUMERIC_RANDOM = "\"ALPHANUMERIC RANDOM\"";
  public static final String NUMERIC_RANDOM = "\"NUMERIC RANDOM\"";
  public static final String UUID = "\"UUID\"";
  public static final String NON_EMPTY = "NON-EMPTY";
  // Used in Step definition
  public static final String URL = ".url";
  public static final String HTTP_METHOD = ".httpMethod";
  public static final String BASE_URL = "baseUrl";
  public static final String JSON_FILE = ".json";
  public static final String MEMORY = "memory";
  public static final String CONTENT_TYPE = "Content-Type";
  public static final String JSON_APPLICATION = "application/json";

}
