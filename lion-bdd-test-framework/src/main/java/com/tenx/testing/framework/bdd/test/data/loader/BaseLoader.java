package com.tenx.testing.framework.bdd.test.data.loader;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.tenx.lion.utility.conversion.JsonUtils;
import com.tenx.testing.framework.bdd.test.config.Config;

@Component
public class BaseLoader {

  static private ConcurrentHashMap<String, Class<?>> classInstanceMap = new ConcurrentHashMap<>();

  private static final String JSON_EXTENSION = ".json";

  @Autowired
  private Config config;

  /**
   * Load the Entity from Json content.
   * 
   * @param entity the entity to load
   * @param content json content
   * @return the entity
   * @throws JsonParseException json parse exception
   * @throws JsonMappingException json mapping exception
   * @throws IOException IO exception
   * @throws ClassNotFoundException class not found exception
   */
  public <T> T loadEntityFromJsonContent(String entity, String content)
      throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {
    String fullyQualifiedEntityPath = getFullyQualifiedClassPath(entity);
    Class<T> target = (Class<T>) getClassInstance(entity, fullyQualifiedEntityPath);
    return JsonUtils.stringToJson(content, target);
  }

  /**
   * Load the Entity from Json file.
   * 
   * @param entity entity the entity to load
   * @param jsonFileName json file name
   * @return the entity
   * @throws JsonParseException json parse exception
   * @throws JsonMappingException json mapping exception
   * @throws IOException IO exception
   * @throws ClassNotFoundException class not found exception
   */
  public <T> T loadEntityFromJsonFile(String entity, String jsonFileName)
      throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {
    String entityJsonPath = getJsonPath(entity);
    String targetFileName = jsonFileName;
    if (jsonFileName == null) {
      String fullyQualifiedEntityPath = getFullyQualifiedClassPath(entity);
      Class<T> target = (Class<T>) getClassInstance(entity, fullyQualifiedEntityPath);
      targetFileName = target.getSimpleName();
    }
    return loadEntityFromJsonContent(entity, readJsonFileAsString(entityJsonPath, targetFileName));
  }

  /**
   * Load the Entity from Json.
   * 
   * @param entity the entity to load
   * @return the entity
   * @throws JsonParseException json parse exception
   * @throws JsonMappingException json mapping exception
   * @throws IOException IO exception
   * @throws ClassNotFoundException class not found exception
   */
  public <T> T loadEntityFromJson(String entity)
      throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {
    return loadEntityFromJsonFile(entity, null);
  }

  /**
   * Get the JSON string from the file.
   * 
   * @param fileName json file name
   * @return json as a string
   * @throws IOException IO exception.
   */
  private String getJsonString(String path, String fileName) throws IOException {
    ClassLoader loader = ClassLoader.getSystemClassLoader();
    return IOUtils.toString(loader.getResourceAsStream(
        (StringUtils.isBlank(path) ? "" : path + "/")
            + fileName.concat(fileName.endsWith(JSON_EXTENSION) ? "" : JSON_EXTENSION)));
  }

  public String readJsonFileAsString(String path, String fileName) throws IOException {
    return getJsonString(path, fileName);
  }

  public JSONObject readJsonFileAsObject(String path, String fileName)
      throws IOException, JSONException {
    return new JSONObject(getJsonString(path, fileName));
  }

  /**
   * Get the entity's instance if available in the hash.
   * 
   * @param fullyQualifiedClassPath entity's fully qualified name.
   * @return instance of the entity class.
   * @throws ClassNotFoundException class not found exception.
   */
  public Class<?> getClassInstance(String className, String fullyQualifiedClassPath)
      throws ClassNotFoundException {
    // Get the instance form the map if already available.
    Class<?> classInstance = classInstanceMap.get(className);
    if (classInstance == null) {
      classInstance = Class.forName(fullyQualifiedClassPath);
      classInstanceMap.put(className, classInstance);
    }
    return classInstance;
  }

  /**
   * Get the entity's instance if available in the hash.
   *
   * @param className class fully qualified name.
   * @return instance of the class.
   * @throws ClassNotFoundException class not found exception.
   */
  public Class<?> getClassInstance(String className) throws ClassNotFoundException {
    return getClassInstance(className, getFullyQualifiedClassPath(className));
  }

  /**
   * Convert a string starting with Caps to the standard way. eg:
   * 
   * @param value string that needs to be converted to standards.
   * @return string that is converted to standards.
   */
  private String toStandards(String value) {
    char c[] = value.toCharArray();
    c[0] = Character.toLowerCase(c[0]);
    return new String(c);
  }

  /**
   * Get the json path.
   * 
   * @param jsonName name of the json for which we need the json path.
   * @return path where the json exists.
   */
  public String getJsonPath(String jsonName) {
    return getConfigValue(toStandards(jsonName).concat(JSON_EXTENSION));
  }

  /**
   * Get the class fully qualified path.
   * 
   * @param clazz clazz for which we need the fully qualified path.
   * @return fully qualified path of the clazz.
   */
  public String getFullyQualifiedClassPath(String clazz) {
    return getConfigValue(toStandards(clazz).concat(".class"));
  }

  /**
   * Get the Queue Name
   *
   * @param messageType type of message for which the queue name is required
   * @return queue name associated with the message type
   */
  public String getQueueName(String messageType) {
    return getConfigValue(toStandards(messageType).concat(".queue"));
  }

  /**
   * Get the queuename to which the message needs to be posted.
   */
  public String getQueueName(String messageType, String queueType) {
    return getConfigValue(toStandards(messageType).concat(queueType));
  }

  /**
   * Get the value from properties file for a given key.
   *
   * @param key key for which the vaue is required
   * @return value for the given key
   */
  private String getConfigValue(String key) {
    return config.get(key);
  }
}
