package com.tenx.testing.framework.bdd.test.repository;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.FailedBatch;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.datamodeling.QueryResultPage;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.tenx.lion.utility.logging.LionDefaultLoggerFactory;
import com.tenx.lion.utility.logging.LionLogger;
import com.tenx.testing.framework.bdd.test.data.loader.BaseLoader;

@Component
public class CucumberRepositoryPool {

  @Value("${amazon.dynamodb.endpoint}")
  private String dynamoDbEndpoint;

  private String defautRegion = "eu-west-1";

  private AmazonDynamoDB client;

  private DynamoDBMapper mapper;

  @Autowired
  private BaseLoader baseLoader;

  private static final LionLogger LOGGER =
      LionDefaultLoggerFactory.getLogger(CucumberRepositoryPool.class);

  @PostConstruct
  public void initialize() {
    client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
        new AwsClientBuilder.EndpointConfiguration(dynamoDbEndpoint, defautRegion)).build();
    mapper = new DynamoDBMapper(client);
  }

  /**
   * This method is used to add items to the database.
   * 
   * @param List<S> List of items to be added
   * @return Failed items of each table.
   */
  public <S> Map<String, List<String>> saveItems(List<S> items) {
    List<FailedBatch> failedBatchList = mapper.batchSave(items);

    if (!failedBatchList.isEmpty()) {
      Map<String, List<String>> resultMap = new LinkedHashMap<String, List<String>>();
      failedBatchList.forEach(p -> p.getUnprocessedItems().keySet()
          .forEach(q -> p.getUnprocessedItems().get(q).stream().forEach(request -> {
            List<String> objects = new ArrayList<String>();
            objects.add(request.getPutRequest().getItem().toString());
            if (resultMap.containsKey(q)) {
              objects.addAll(resultMap.get(q));
            }
            resultMap.put(q, objects);
          })));
      return resultMap;
    }

    return null;
  }

  @SuppressWarnings("unchecked")
  public <S> Map<String, List<String>> saveItem(S item) {
    List<S> items = new ArrayList<S>();
    items.add(item);
    return saveItems(items);
  }

  /**
   * This method is used to retrieve the saved record/data from the dataabase based on hashkey and
   * rangekey.
   * 
   * @param id
   * @param entityName
   * @return  Object
   * @throws ClassNotFoundException
   */
  @SuppressWarnings({"rawtypes", "unchecked"})
  public Object retrieve(ID id, String entityName) throws ClassNotFoundException {

    Class<?> className = baseLoader.getClassInstance(entityName);

    populateDynamoEntityId(className, id);

    AttributeValue hashKeyAttribute = new AttributeValue();
    AttributeValue rangeKeyAttribute = new AttributeValue();

    setAttributeValue(id.getHaskKey(), hashKeyAttribute, id.getHashKeyType());
    setAttributeValue(id.getRangeKey(), rangeKeyAttribute, id.getRangeKeyType());

    Object result = null;

    Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
    expressionAttributeValues.put(":hashKey", hashKeyAttribute);
    expressionAttributeValues.put(":rangeKey", rangeKeyAttribute);

    DynamoDBQueryExpression queryExpression = new DynamoDBQueryExpression<>()
        .withKeyConditionExpression(
            id.getHashkeyName() + "= :hashKey and " + id.getRanageKeyName() + " = :rangeKey")
        .withExpressionAttributeValues(expressionAttributeValues);

    QueryResultPage<?> recordList = mapper.queryPage(className, queryExpression);

    if (recordList.getResults() != null) {
      result = recordList.getResults().get(0);
    }
    return result;
  }

  /**
   * Delete the resource from dynamo db.
   * 
   * @param item item to be deleted
   * @return <code>true</true> if success, </br>
   *        <code>false</code> otherwise
   */
  public <S> void delete(List<S> itemsToDelete) {
    itemsToDelete.stream().forEach(delete -> {
      try {
        LOGGER.info("Attempting to delete Resource : " + delete.getClass().getName());
        mapper.delete(delete);
      } catch (Exception e) {
        LOGGER.info("Attempting to delete Resource : " + delete.getClass().getName()
            + " has failed with message : " + e.getLocalizedMessage());
      }
    });
  }

  // set attribute values based on field type
  private void setAttributeValue(String key, AttributeValue attribute, Class<?> type) {
    if (type.getName().equals(Long.class.getName())) {
      attribute.withN(key);
    } else {
      attribute.withS(key);
    }
  }

  // to build the dynamo Id (hashkey and range key)
  private ID populateDynamoEntityId(Class<?> className, ID id) {

    boolean isIdFound = false;
    for (Class<?> c = className; c != null; c = c.getSuperclass()) {
      Method[] methods = c.getMethods();

      for (Method method : methods) {
        if (method.isAnnotationPresent(DynamoDBHashKey.class)) {
          isIdFound = true;

          DynamoDBHashKey hashKey = method.getAnnotation(DynamoDBHashKey.class);

          id.setHashkeyName(hashKey.attributeName());

          Class<?> methodReturnType = method.getReturnType();
          id.setHashKeyType(methodReturnType);

        }
        if (method.isAnnotationPresent(DynamoDBRangeKey.class)) {
          isIdFound = true;
          DynamoDBRangeKey rangeKey = method.getAnnotation(DynamoDBRangeKey.class);
          id.setRanageKeyName(rangeKey.attributeName());

          Class<?> methodReturnType = method.getReturnType();
          id.setRangeKeyType(methodReturnType);

        }
      }
      if (isIdFound) {
        break;
      }
    }
    return id;
  }

  // check if given column is one of the id(hashKey or rangekey)
  private String getFieldTypeForIdByFieldName(String columnName, Field field) {

    Class<?> idClass = field.getType();

    String columnNameFromMethodAnnotation =
        getFieldTypeByMethodByIdAnnotation(idClass.getMethods(), columnName);

    if (StringUtils.isEmpty(columnNameFromMethodAnnotation)) {
      columnNameFromMethodAnnotation =
          getFieldTypeByFieldAnnotation(idClass.getDeclaredFields(), columnName);
    }
    return columnNameFromMethodAnnotation;
  }

  // check if the DynamoHashKeyAnnotation present on fields and column is
  // matched with annotation
  private String getFieldTypeByFieldAnnotation(Field[] fields, String columnName) {

    for (Field field : fields) {
      if (field.isAnnotationPresent(DynamoDBHashKey.class)) {

        DynamoDBHashKey hashKey = field.getAnnotation(DynamoDBHashKey.class);

        if (isFieldNameEqual(columnName, hashKey.attributeName())) {
          return field.getType().getName();
        }

      }
      if (field.isAnnotationPresent(DynamoDBRangeKey.class)) {

        DynamoDBRangeKey rangeKey = field.getAnnotation(DynamoDBRangeKey.class);

        if (isFieldNameEqual(columnName, rangeKey.attributeName())) {
          return field.getType().getName();
        }

      }
    }

    return null;

  }

  // check if the DynamoHashKeyAnnotation present on methods and column is
  // matched with annotation
  private String getFieldTypeByMethodByIdAnnotation(Method[] methods, String columnName) {
    String fieldType = null;
    for (Method method : methods) {
      if (method.isAnnotationPresent(DynamoDBHashKey.class)) {

        DynamoDBHashKey hashKey = method.getAnnotation(DynamoDBHashKey.class);

        if (isFieldNameEqual(columnName, hashKey.attributeName())) {
          return method.getReturnType().getName();
        }

      }
      if (method.isAnnotationPresent(DynamoDBRangeKey.class)) {

        DynamoDBRangeKey rangeKey = method.getAnnotation(DynamoDBRangeKey.class);

        if (isFieldNameEqual(columnName, rangeKey.attributeName())) {
          fieldType = method.getReturnType().getName();
          break;
        }

      }
    }
    return fieldType;
  }

  private boolean isFieldNameEqual(String columnName, String keyName) {
    return (columnName.equals(keyName) ? true : false);
  }

  // get the field type by column name, it checks including hash key and range key
  private String getFieldTypeByColumnName(Class<?> className, String columnName) {
    String fieldType = null;

    for (Class<?> c = className; c != null; c = c.getSuperclass()) {

      Field[] fields = c.getDeclaredFields();
      fieldType = getFieldType(fields, columnName);
      if(!StringUtils.isEmpty(fieldType)) {
        break;
      }
    }

    if (StringUtils.isEmpty(fieldType)) {
      fieldType = getFieldTypeByMethodAnnotation(className, columnName);
    }
    return fieldType;
  }

  private String getFieldType(Field[] fields, String columnName) {
    String fieldType = null;

    for (Field field : fields) {

      if (field.isAnnotationPresent(DynamoDBAttribute.class)) {
        DynamoDBAttribute fieldAnnotation = field.getAnnotation(DynamoDBAttribute.class);
        if (fieldAnnotation.attributeName().equals(columnName)) {
          fieldType = field.getType().getName();
        }
      } else if (field.isAnnotationPresent(Id.class)) {
        fieldType = getFieldTypeForIdByFieldName(columnName, field);
      }
      if (!StringUtils.isEmpty(fieldType)) {
        break;
      }
    }
    return fieldType;
  }

  private String getFieldTypeByMethodAnnotation(Class<?> className, String columnName) {
    String fieldType = null;
    for (Class<?> c = className; c != null; c = c.getSuperclass()) {

      Method[] methods = c.getDeclaredMethods();
      for (Method method : methods) {
        if (method.isAnnotationPresent(DynamoDBAttribute.class) &&
            isFieldNameEqual(columnName, method.getAnnotation(DynamoDBAttribute.class).attributeName()) ) {
            fieldType = method.getReturnType().getName();
            break;
        }
      }
      if(!StringUtils.isEmpty(fieldType)) {
        break;
      }
    }

    return fieldType;

  }

  /**
   * 
   * @param columnNameValueMap
   * @param entityName
   * @return Object based in column names
   * @throws ClassNotFoundException
   */
  public List<Object> retrieveByColumnNames(Map<String, String> columnNameValueMap,
      String entityName) throws ClassNotFoundException {

    Class<?> className = baseLoader.getClassInstance(entityName);
    DynamoDBScanExpression queryExpression = new DynamoDBScanExpression();

    Map<String, AttributeValue> expressionAttributeValues =
        populateAttributeValues(columnNameValueMap, className);

    queryExpression.withFilterExpression(buildFilterExpression(expressionAttributeValues))
        .withExpressionAttributeValues(expressionAttributeValues);

    PaginatedScanList<?> results = mapper.scan(className, queryExpression);
    List<Object> resultObjects = results.stream().collect(Collectors.toList());
    return resultObjects;
  }

  private String buildFilterExpression(Map<String, AttributeValue> expressionAttributeValues) {
    StringBuilder filterExpression = new StringBuilder();
    final int filterStart = 5;

    expressionAttributeValues.keySet().stream().forEach(key -> {
      filterExpression.append(" and " + key.substring(1, key.length()) + " = " + key);
    });

    return filterExpression.substring(filterStart, filterExpression.length());
  }

  // populate attributes for query condition based on field types
  private Map<String, AttributeValue> populateAttributeValues(
      Map<String, String> columnNameValueMap, Class<?> className) {

    Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();

    columnNameValueMap.keySet().stream().forEach(key -> {
      String value = columnNameValueMap.get(key);
      String fieldType = getFieldTypeByColumnName(className, key);
      final String LONG_TYPE = Long.class.getName();

      if (fieldType.equals(LONG_TYPE)) {
        expressionAttributeValues.put(":" + key, new AttributeValue().withN(value));
      } else {
        expressionAttributeValues.put(":" + key, new AttributeValue().withS(value));
      }
    });
    return expressionAttributeValues;
  }

  public static class ID {
    private String hashkeyName;
    private String ranageKeyName;
    private String haskKey;
    private String rangeKey;
    private Class<?> hashKeyType;
    private Class<?> rangeKeyType;

    public Class<?> getHashKeyType() {
      return hashKeyType;
    }
    public void setHashKeyType(Class<?> hashKeyType) {
      this.hashKeyType = hashKeyType;
    }
    public Class<?> getRangeKeyType() {
      return rangeKeyType;
    }
    public void setRangeKeyType(Class<?> rangeKeyType) {
      this.rangeKeyType = rangeKeyType;
    }
    public String getHashkeyName() {
      return hashkeyName;
    }
    public void setHashkeyName(String hashkeyName) {
      this.hashkeyName = hashkeyName;
    }
    public String getRanageKeyName() {
      return ranageKeyName;
    }
    public void setRanageKeyName(String ranageKeyName) {
      this.ranageKeyName = ranageKeyName;
    }
    public String getHaskKey() {
      return haskKey;
    }
    public void setHaskKey(String haskKey) {
      this.haskKey = haskKey;
    }
    public String getRangeKey() {
      return rangeKey;
    }
    public void setRangeKey(String rangeKey) {
      this.rangeKey = rangeKey;
    }
  }
}
