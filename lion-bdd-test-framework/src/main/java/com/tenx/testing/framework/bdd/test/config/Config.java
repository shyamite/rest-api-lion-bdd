package com.tenx.testing.framework.bdd.test.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.annotation.ProfileValueSource;

@Configuration
public class Config implements ProfileValueSource {

	private static final String TEST_CONFIG_PROPERTIES_FILE_NAME = "config/test_config.properties";

	private static Properties testConfigFile;
	static {
		InputStream configFileInputStream = Config.class.getClassLoader()
				.getResourceAsStream(TEST_CONFIG_PROPERTIES_FILE_NAME);
		testConfigFile = new Properties();
		try {
			testConfigFile.load(configFileInputStream);
		} catch (IOException e) {
		} finally {
			if (configFileInputStream != null) {
				try {
					configFileInputStream.close();
				} catch (IOException e) {
				}
			}
		}

	}

	@Override
	public String get(String key) {
		return testConfigFile.getProperty(key);
	}

}
