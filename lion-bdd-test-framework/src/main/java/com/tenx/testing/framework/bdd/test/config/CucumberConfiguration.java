package com.tenx.testing.framework.bdd.test.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@ComponentScan(basePackages = {"com.tenx.*"})
@PropertySource({"classpath:application.properties","classpath:/config/test_config.properties"})
public class CucumberConfiguration {

}
