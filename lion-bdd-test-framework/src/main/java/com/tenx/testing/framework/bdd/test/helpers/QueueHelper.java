package com.tenx.testing.framework.bdd.test.helpers;

import javax.jms.JMSException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;


@Component
public class QueueHelper {

  @Autowired
  private JmsTemplate jmsTemplate;

  public void postMessageToSQSQueue(String message, String queueName) throws JMSException {
    jmsTemplate.convertAndSend(queueName, message);
  }
}
